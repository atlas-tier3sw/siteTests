#! /bin/bash 
#!----------------------------------------------------------------------------
#!
#! configureScripts.sh
#!
#! Configure the Panda or Ganga scripts for submissions
#!
#! Usage: 
#!     configureScripts.sh  --help
#!
#! History:
#!   18Jul16: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

st_progname="pandaJobs"

#!----------------------------------------------------------------------------
st_fn_configureScriptsHelp()
# Simple help to stdout
#!----------------------------------------------------------------------------
{
    \cat <<EOF

Usage: $st_progname [options]

   Configure Panda or Ganga scripts for submission

   return code with path to script or error message

    Mandatory options
    -d --dir                      dir where the results are to be found
    -p --panda                    sonfigure for Panda (not needed if ganga)
    -g --ganga                    sonfigure for Ganga (not needed if panda)
    -i --inDS=STRING              input dataset or pattern
    -q --queue=STRING             analy queue for submission
    -t --template=STRING          template for panda or ganga jobs    
    -r --runDir                   Run dir where scripts are located
    -u --user                     User nickname (RUCIO_ACCOUNT)
  
    Options (to override defaults) are:
    -h  --help                    print this help message
    -s --script                   run testjob.sh script file for grid
    -a --addPandaOpts             additional pathena/prun options 

EOF
}


#!----------------------------------------------------------------------------
st_fn_createPandaJob()
#!----------------------------------------------------------------------------
{

    local st_host=`hostname -s`
    local st_uuid=`uuidgen`
    if [ "$st_script" = "" ]; then 
	local st_outputds="user.$RUCIO_ACCOUNT.p.a.$st_template.$st_site.$st_host.$st_uuid"
    else
	local st_outputds="user.$RUCIO_ACCOUNT.p.t.$st_template.$st_site.$st_host.$st_uuid"
    fi

    \cat <<EOF > $st_dir/submitTheJob.sh

    st_script="$st_script"
    st_site="$st_site"
    st_template="$st_template"
    st_queue="$st_queue"
    st_dataset="$st_dataset"
    st_addPandaOpts="$st_addPandaOpts"
    st_runDir="$st_runDir"
    st_outputds="$st_outputds"
    
    if [ "$st_script" = "" ]; then
        if [ -e $st_runDir/my_pandaJob.sh ]; then
	  source $st_runDir/my_pandaJob.sh
        else
	  source $st_runDir/pandaJob.sh
        fi
    else
        if [ -e $st_runDir/my_pandaTestJob.sh ]; then
  	  source $st_runDir/my_pandaTestJob.sh
        else
  	  source $st_runDir/pandaTestJob.sh
        fi
    fi

    unset st_script st_queue st_dataset st_site st_template st_addPandaOpts st_runDir st_user st_outputds

EOF

    return 0
}


#!----------------------------------------------------------------------------
st_fn_createGangaJob()
#!----------------------------------------------------------------------------
{

    local st_host=`hostname -s`
    local st_uuid=`uuidgen`
    if [ "$st_script" = "" ]; then 
	local st_outputds="g.a.$st_template.$st_site.$st_host.$st_uuid"
	local st_gangaName="$st_template.a.$st_site"
    else
	local st_outputds="g.t.$st_template.$st_site.$st_host.$st_uuid"
	local st_gangaName="$st_template.t.$st_site"
    fi

    \cat <<EOF > $st_dir/submitTheJob.py

st_script = '$st_script'
st_site = '$st_site'
st_template = '$st_template'
st_queue = '$st_queue'
st_dataset = '$st_dataset'
st_addPandaOpts = '$st_addPandaOpts'
st_runDir = '$st_runDir'
st_outputds = '$st_outputds'
st_gangaName = '$st_gangaName'
EOF

    if [ "$st_script" = "" ]; then 
	if [ -e $st_runDir/my_gangaJob.py ]; then
	    \cat $st_runDir/my_gangaJob.py >> $st_dir/submitTheJob.py
	else
	    \cat $st_runDir/gangaJob.py >> $st_dir/submitTheJob.py
	fi
    else
	if [ -e $st_runDir/my_gangaTestJob.py ]; then
	    \cat $st_runDir/my_gangaTestJob.py >> $st_dir/submitTheJob.py
	else
	    \cat $st_runDir/gangaTestJob.py >> $st_dir/submitTheJob.py
	fi
    fi
    
    return 0
}


#!----------------------------------------------------------------------------
#! main
#!----------------------------------------------------------------------------


st_shortopts="h,p,g,i:,q:,t:,s,d:,a:,r:,u:"
st_longopts="help,panda,ganga,inDS:,queue:,template:,script,dir:,addPandaOpts:,runDir;,user:"
st_opts=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_parseOptions.sh bash $st_shortopts $st_longopts $st_progname "$@"`
if [ $? -ne 0 ]; then
    \echo $st_opts 1>&2
    exit 64
fi
eval set -- "$st_opts"

st_panda="NO"
st_ganga="NO"
st_inDS=""
st_queue=""
st_template=""
st_script=""
st_dir=""
st_addPandaOpts=""
st_runDir=""
st_user=""

while [ $# -gt 0 ]; do
    : debug: $1
    case $1 in
        -h|--help)
            st_fn_configureScriptsHelp
            exit 0
            ;;
	-a|--addPandaOpts)
	    st_addPandaOpts="$2"
	    shift 2
	    ;;
	-u|--user)
	    st_user="$2"
	    shift 2
	    ;;
	-r|--runDir)
	    st_runDir="$2"
	    shift 2
	    ;;
	-d|--dir)
	    st_dir="$2"
	    shift 2
	    ;;
        -p|--panda)
            st_panda="YES"
            shift
            ;;
        -g|--ganga)
            st_ganga="YES"
            shift
            ;;
        -i|--inDS)
            st_inDS="$2"
            shift 2
            ;;
        -q|--queue)
            st_queue="$2"
            shift 2
            ;;
        -t|--template)
            st_template="$2"
            shift 2
            ;;
        -s|--script)
	    if [ -e $st_runDir/my_testJob.sh ]; then
		st_script="$st_runDir/my_testJob.sh"
	    else
		st_script="testJob.sh"
	    fi
            shift 
            ;;
	--)
            shift
            break
            ;;
        *)
            \echo "Internal Error: option processing error: $1" 1>&2
            exit 1
            ;;
    esac
done

if [ "$st_dir" = "" ]; then
    \echo "Error: you neeed to specify the dir" 
    exit 64
elif [ "$st_runDir" = "" ]; then
    \echo "Error: you neeed to specify the run dir" 
    exit 64
elif [ "$st_queue" = "" ]; then
    \echo "Error: you neeed to specify the queue" 
    exit 64
elif [[ $st_queue =~ "," ]]; then
    \echo "Error: You can specify only one queue"
    exit 64
elif [[ ! $st_queue =~ "ANALY" ]]; then
    \echo "Error: The queue needs to be an ANALY_XXX queue"
    exit 64
elif [ "$st_template" = "" ]; then
    \echo "Error: you neeed to specify the template" 
    exit 64
elif [ -z $RUCIO_ACCOUNT ]; then
    \echo "Error: you neeed to first define RUCIO_ACCOUNT environment variable"
    exit 64
elif [[ "$st_panda" = "" ]] && [[ "$st_ganga" = "" ]]; then
    \echo "Error: you need to specify either panda or ganga option"
fi

st_queue=$(\echo $st_queue | tr 'a-z' 'A-Z')

shopt -s expand_aliases

source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh $st_AlrbOpts --quiet --noLocalPostSetup

lsetup agis rucio -q

# get a proxy if needed
st_doProxy=""
voms-proxy-info -exists > /dev/null 2>&1
if [ $? -ne 0 ]; then
    st_doProxy="Y"
else
    st_result=`voms-proxy-info --actimeleft 2>&1 | \grep -e "^[0-9]*$"`
    if [[ $? -ne 0 ]] || [[ "st_result" = "" ]]; then
	st_doProxy="Y"
    fi
fi

if [ "$st_doProxy" = "Y" ]; then
    \echo ""
    \echo "Getting a valid proxy ..."
    voms-proxy-init -voms atlas -valid 96:0
    if [ $? -ne 0 ]; then
	exit 64
    fi
fi

# check if queue is real
st_validQueue="NO"
st_site=""
\rm -f $st_dir/agisQueueList.txt
agis-list-panda-resources --panda-queue=$st_queue --output=site > $st_dir/agisQueueList.txt 2>&1
if [ $? -eq 0 ]; then
    st_site=`\egrep -v "#" $st_dir/agisQueueList.txt | \sed -e 's/ //g'`
    if [ $? -eq 0 ]; then
	st_validQueue="YES"
    fi
fi
if [ "$st_validQueue" != "YES" ]; then
    \echo "Error in obtaining site for queue $st_queue"
    \echo " Could the queue name be wrong ?  Possibilities:"
    agis-list-panda-queues --output=name | \grep -i $st_queue
    exit 64
fi

st_skipData="NO"
if [ "$st_script" != "" ]; then
    st_result=`\grep st_dataset $st_script 2>&1`
    if [ $? -ne 0 ]; then
	st_skipData="YES"
    fi
fi
st_dataset=""
if [[ "$st_inDS" != "" ]] && [[ "$st_skipData" != "YES"  ]]; then
    st_result=`\echo $st_inDS | \grep -e "\*"`
    if [ $? -eq 0 ]; then
	st_inDSAr=()
	for st_item in `\echo "$st_inDS" | \sed -e 's/,/ /g'`; do	    
	    st_inDSAr=( ${st_inDSAr[@]} `rucio list-dids $st_item --short` ) 
	    if [ $? -ne 0 ]; then
		\echo "Error obtaining dataset information."
		\echo "  $st_item"
		exit 64
	    fi
	done
    else
	st_inDSAr=( `\echo "$st_inDS" | \sed -e 's/,/ /g'` )
    fi
    for st_item in ${st_inDSAr[@]}; do    	
	st_didAr=( `rucio list-dids $st_item | \grep DATASET | \cut -f 2 -d "|"` ) 
	if [ $? -ne 0 ]; then
	    \echo "Error obtaining dataset information."
	    \echo "${st_dataAr[@]}"
	    exit 64
	fi		

	for st_did in ${st_didAr[@]}; do
	    st_result=`rucio list-dataset-replicas $st_did | \egrep -v TAPE |  \grep -e $st_site`
	    if [[ $? -eq 0 ]] && [[ "$st_result" != "" ]]; then
		st_dataset=$st_did
		break 2
	    fi
	done    
    done

fi

if [[ "$st_script" = "" ]] && [[ "$st_dataset" = "" ]]; then
    \echo "Error: unable to find a dataset from pattern $st_inDS at the site $st_site"
    exit 64
fi

if [ "$st_panda" != "NO" ]; then
    st_fn_createPandaJob
    exit $?
elif [ "$st_ganga" != "NO" ]; then
    st_fn_createGangaJob
    exit $?
fi

exit 0

#!----------------------------------------------------------------------------
#! purgeJobs.py
#! 
#! A simple script to cleanup jobs - ie. remove them from being listed.
#!
#! Usage: This is designed to run from within Ganga.  At the ganga prompt, type
#!  execfile("purgeJobs.py") and respond to the queries.
#!
#! History:
#!  05Oct08 A. De Silva, First version.
#!  
#!----------------------------------------------------------------------------

st_query = " ".join([" Lowest job number to delete ? : "])
st_response = raw_input(st_query)
if st_response is not None:
    try:
        st_ia = int(st_response)
    except ValueError:
        print "Error: repeat needs an integer argument."
        sys.exit(64)
    if st_ia > 0:
        st_purgeLowerLimit = st_ia
    else:
        print "Error: purge days needs a positive argument."
        sys.exit(64)
else:
    print "Error: You need to specify a value"
    sys.exit(64)

st_query = " ".join([" Highest job number to delete ? : "])
st_response = raw_input(st_query)
if st_response is not None:
    try:
        st_ia = int(st_response)
    except ValueError:
        print "Error: repeat needs an integer argument."
        sys.exit(64)
    if st_ia >= 0:
        st_purgeUpperLimit = st_ia+1
    else:
        print "Error: purge days needs a positive argument."
        sys.exit(64)
else:
    print "Error: You need to specify a value"
    sys.exit(64)


for st_idx in range (st_purgeLowerLimit, st_purgeUpperLimit):
    try:
        jobs(st_idx).remove()
    except:
        print 'Ignoring job ',st_idx
        pass

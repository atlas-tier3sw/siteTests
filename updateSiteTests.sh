#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! updateSiteTests.sh 
#!
#! A script to do updates of siteTests; you can make this a cron job.
#!
#! Usage: 
#!     updateSiteTests.sh
#!
#! History:
#!   17Feb09: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

st_t2SWDir=`\dirname $0`
st_t2SWDir=`readlink -f $st_t2SWDir`
cd $st_t2SWDir

\mkdir -p $HOME/.SiteTests
if [ ! -e $HOME/.SiteTests/config.sh ]; then
    \echo "st_t2SWDir=$st_t2SWDir" > $HOME/.SiteTests/config.sh
else
    \mv $HOME/.SiteTests/config.sh $HOME/.SiteTests/config.sh.old
    \echo "st_t2SWDir=$st_t2SWDir" > $HOME/.SiteTests/config.sh
fi

st_svnRoot="http://svn.cern.ch/guest/atcansupport/"

# check that we are in siteTests
if [ ! -e $st_t2SWDir/create.sh ]; then
    \echo -e "You need to run this in the siteTests directory.          "'[\033[33mFAILED\033[0m]'
    exit 64
fi 

# get the version that we need to upgrade 
\echo "Need to get trunk for latestVersion ..."
svn switch $st_svnRoot/siteTests/trunk $st_t2SWDir
if [ $? -eq 0 ]; then
    st_siteTestVersion=`\cat $st_t2SWDir/latestVersion`
    svn switch $st_svnRoot/siteTests/tags/$st_siteTestVersion $st_t2SWDir
    if [ $? -ne 0 ]; then
	\echo -e "Fetch required version                                    "'[\033[31mFAILED\033[0m]'
	exit 64
    else
	\echo -e "Fetch required version                                    "'[\033[32m  OK  \033[0m]'
    fi
else
    \echo -e "Fetch latest version                                      "'[\033[31mFAILED\033[0m]'
    exit 64
fi




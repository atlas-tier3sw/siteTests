#! /bin/bash

\echo 'testJob.sh running...'
\echo "----------------------"
\echo "date is " `date`
\echo "hostname is " `hostname -f`
\echo "id is " `id`
\echo "uname is : " `uname -a` 
\echo "pwd is : " `pwd`
\echo "----------------------"
\echo "Voms proxy is : "
voms-proxy-info -all
\echo "----------------------"
\echo "Env is : "
env
\echo "----------------------"
\echo "Site specific tests :"

\echo "Add anything you want here ..."

\echo "----------------------"

exit 0

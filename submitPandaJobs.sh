#!----------------------------------------------------------------------------
#!
#! submitPandaJobs.sh
#!
#! A script for submitting pathena / prun jobs 
#!
#! Usage: 
#!     source submitPandaJobs.sh -h
#!
#! History:
#!   19Nov08: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

if [ -z $SITETESTS_DEFINED_PANDA ]; then
    lsetup panda
    export SITETESTS_DEFINED_PANDA="YES"
fi

if [ ! -d $ALRB_tmpScratch ]; then
    \echo "Error: temporary scratch dir (ALRB_tmpScratch) does not exist"
    exit 64
fi
st_workarea=`\mktemp -d $ALRB_tmpScratch/siteTests.XXXXXX`
if [ $? -ne 0 ]; then
    \echo "Failed to create dir"
    exit 64
fi

\rm -f $st_workarea/configure.sh
env | \grep -e "^ALRB" -e "^ATLAS_LOCAL_" -e "^X509_USER_PROXY" -e "^RUCIO_ACCOUNT" |  env LC_ALL=C \sort | \awk '{print "export "$1""}' | \sed -e 's|=|="|g' -e 's|$|"|g' > $st_workarea/configure.sh
\echo "export HOME=$HOME; $ST_t2SWDir/configureScripts.sh --panda -i $st_inDS -t $st_Template -d $st_workarea -r `pwd` $@" >> $st_workarea/configure.sh
chmod +x $st_workarea/configure.sh
env -i bash -l -c $st_workarea/configure.sh

if [ $? -eq 0 ]; then
    source $st_workarea/submitTheJob.sh
fi

\rm -rf $st_workarea

unset st_workarea

return 0






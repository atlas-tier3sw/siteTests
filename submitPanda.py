#!----------------------------------------------------------------------------
#! submitGangaPanda.py
#! 
#! A simple script to submit Gangajobs to the Panda backend.
#!
#! Usage: This is designed to run from within Ganga.  At the ganga prompt, type
#!  execfile("submitPanda.py") and respond to the queries.
#!
#! History:
#!  26Nov08 A. De Silva, First version.
#!          Tried using getopt but ganga/ipython is not option friendly.
#!          So the workaround is to query the options with defaults.
#!  
#!----------------------------------------------------------------------------

import os, re, subprocess, stat

ST_workarea = os.getenv("ST_workarea")
ST_t2SWDir = os.getenv("ST_t2SWDir")
ST_inDS = os.getenv("ST_inDS")
ST_template = os.getenv("ST_template")
ST_testArea = os.getenv("TestArea")
ST_Home = os.getenv("HOME")

st_runDir = os.getcwd()

st_doAthena=True
st_response = raw_input("Is this an AFT HC template job [Y] : ") 
if st_response != "" : 
    if re.search(r'^[Yy]$|^[Yy][Ee][Ss]$',st_response):
        st_doAthena=True
    elif re.search(r'^[Nn]$|^[Nn][Oo]$',st_response):
        st_doAthena=False
    else:
        print "Error: Please type Y or N"
        sys.exit(64)

st_queue = ''
st_response = raw_input("".join(["Panda analy queue to run : "]))
if st_response != "":
    st_queue = st_response
else:
    print "Error: You need to type a st_response"
    sys.exit(64)
    
st_configFile="".join([ST_workarea,'/configure.sh'])
try:
    os.remove(st_configFile)
except:
    pass

st_cmd = "".join([ST_t2SWDir,'/configureScripts.sh --ganga -i ',ST_inDS,' -t ',ST_template,' -d ',ST_workarea,' -q ',st_queue,' -r ',st_runDir])
if not st_doAthena:
    st_cmd = "".join([st_cmd,' -s '])
st_fileHandle = open(st_configFile,'w')
st_fileHandle.write("".join(['export HOME=',ST_Home,';']))
st_fileHandle.write(st_cmd) 
st_fileHandle.close()
st_fileHandle = os.stat(st_configFile)
os.chmod(st_configFile, st_fileHandle.st_mode | stat.S_IEXEC | stat.S_IXGRP | stat.S_IXOTH)  

st_cmd = "".join(['env -i bash -l -c ',st_configFile,'\n'])
st_result = subprocess.call(st_cmd, shell=True)

if st_result == 0:
    st_cmdfile = "".join([ST_workarea,'/submitTheJob.py'])
    st_result = subprocess.call(['cat', st_cmdfile])
    execfile(st_cmdfile)


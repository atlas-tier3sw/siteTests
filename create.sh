#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! create.sh
#!
#! creates teh template work dir for siteTests
#!
#! Usage: 
#!     source create.sh --help
#!
#! History:
#!   19Nov08: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

if [ -z $ATLAS_LOCAL_ROOT_BASE ]; then
    \echo "Error: ATLAS_LOCAL_ROOT_BASE is undefined.  Please define it."
    exit 64
fi

st_userDir="$HOME/.SiteTests"
if [ ! -d $st_userDir ]; then
    \echo "Error: did you run updateSiteTests.sh after installation ?"
    exit 64
fi
source $st_userDir/config.sh

st_progname="create.sh"
source $st_t2SWDir/defaultTemplate.sh
st_Template=$st_DefaultTemplate
st_MyWorkHome="${HOME}/public/SiteTestsWork"

st_summaryAr=()

#!----------------------------------------------------------------------------
st_fn_addSummary() 
#!----------------------------------------------------------------------------
{
    st_summaryAr=( "${st_summaryAr[@]}" "$1:$2:$3" ) 

    return 0
}


#!----------------------------------------------------------------------------
st_fn_printSummary() 
#!----------------------------------------------------------------------------
{

    if [ ${#st_summaryAr[@]} -gt 0 ]; then
		\echo  " "
		printf "  %4s %-50s %10s\n" "Step" "Test Description" "Result"
    fi
    local st_item
    for st_item in "${st_summaryAr[@]}"; do
	local st_step=`\echo $st_item | \cut -d ":" -f 1`
	local st_descr=`\echo $st_item | \cut -d ":" -f 2`
	local st_result=`\echo $st_item | \cut -d ":" -f 3`
	printf "  %4s %-50s %10s\n" "$st_step" "$st_descr" "$st_result"
    done
    if [ ${#st_summaryAr[@]} -gt 0 ]; then
		\echo  " "
    fi
}


#!----------------------------------------------------------------------------
st_fn_initialSiteTestsSetupHelp()
# Simple help to stdout
#!----------------------------------------------------------------------------
{
    \cat <<EOF

Usage: $st_progname <template> [options]

    This script sets up the initial environment for Tier2 admins to submit 
    analysis jobs to their site.

    <template> must be a hammercloud template NNN 

    Options (to override defaults) are:
    -h  --help                 Print this help message
    --myWorkHome=STRING        Work dir (Default : $st_MyWorkHome)
    --startStep=INTEGER        Resume from specified step
    --template=STRING          Template (Default: $st_Template )
    --voms=STRING              Voms (default: atlas)
    --alrbOpts=STRING          Options for setupATLAS in ALRB

EOF
}


#!----------------------------------------------------------------------------
st_fn_checkSpace()
#! Check that user has space on lxplus
#! args:
#!----------------------------------------------------------------------------
{
    \echo " " 
    \echo "--> Step $st_Step - Check for disk space"	
    local st_tmpVal
    st_tmpVal=`pwd | \grep -e "^\afs" 2>&1`
    if [ $? -eq 0 ]; then
	local st_used
	st_used=`\fs lq | \grep user | \awk '{print $4}' | \sed -e 's/%//' -e 's/<//g'`
	\echo -ne "Disk space ...                                            "
	if [ $st_used -lt 80 ]; then
	    \echo -e '[\033[32m  OK  \033[0m]'    
	else
	    \echo -e '[\033[31mFAILED\033[0m]'    
	    return 64
	fi    
    fi

    return 0
}

#!----------------------------------------------------------------------------
st_fn_checkHost()
#! Check that host has ALRB available
#! args:
#!----------------------------------------------------------------------------
{
    \echo " "
    \echo "--> Step $st_Step - Check host"
    if [ -z $ATLAS_LOCAL_ROOT_BASE ]; then
	if [ -d "/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase" ]; then
	    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
	else
	    \echo "Error: You either need to run this on lxplus or where ATLAS_LOCAL_ROOT_BASE exists." 
	    \echo -e "Checking host ...                                         "'[\033[31mFAILED\033[0m]'
	    return 64
	fi
    fi
	
    return 0
}


#!----------------------------------------------------------------------------
st_fn_checkGridCertificates()
#! Check that grid certificates exist
#! args:
#!----------------------------------------------------------------------------
{
    \echo 
    \echo "--> Step $st_Step - Check Grid certificates"
    
# are the grid certificates installed in the .globus directory ?    
    local st_usercert=$HOME/.globus/usercert.pem
    local st_userkey=$HOME/.globus/userkey.pem
    \echo -ne "Looking for ~/.globus/usercert.pem ...                    "
    if [ -e $st_usercert ]; then
	\echo -e '[\033[32m  OK  \033[0m]'    
    else
	\echo -e '[\033[31mFAILED\033[0m]'    
	return 64
    fi
    \echo -ne "Looking for ~/.globus/userkey.pem ...                     "
    if [ -e $st_userkey ]; then
	\echo -e '[\033[32m  OK  \033[0m]'    
    else
	\echo -e '[\033[31mFAILED\033[0m]'    
	return 64
    fi    

    return 0
}


#!----------------------------------------------------------------------------
st_fn_createGangaSetup()
#! Create Ganga configuration files
#! args:
#!----------------------------------------------------------------------------
{
    \echo 
    \echo "--> Step $st_Step - Create Ganga Configuration"
    local st_rc=0
    
    if [ ! -e ${st_MyWorkHome}/gangarc ]; then
	\echo "Creating new gangarc file ..."
	source  $ATLAS_LOCAL_ROOT_BASE/swConfig/ganga/generateGangarc.sh --outDir=${st_MyWorkHome} --outFilename=gangarc --voms=${st_Voms} --gangadir="${st_MyWorkHome}/gangadir"
	st_rc=$?
	\echo -ne "Create Ganga Configuration                                "
	if [ $st_rc -eq 0 ]; then
	    \echo -e '[\033[32m  OK  \033[0m]'    
	else
	    \echo -e '[\033[31mFAILED\033[0m]'    
	fi    
    fi

    return $st_rc
}


#!----------------------------------------------------------------------------
st_fn_sourceSetup()
#! setup the release
#! args:
#!----------------------------------------------------------------------------
{
    if [ ! -z $st_RcTagSetup ]; then
	st_fn_sourceASGSetup
    else
	st_fn_sourceAthenaSetup
    fi
    return $?

}



#!----------------------------------------------------------------------------
st_fn_sourceAthenaSetup()
#! asetup the release
#! args:
#!----------------------------------------------------------------------------
{
    \echo 
    \echo "--> Step $st_Step - Setup release"

    local st_rc=0

    cd $st_jobDir
    local st_myasetup="$st_jobDir/.asetup"
    \cat <<EOF > $st_myasetup.tmp
# your test area directory
testarea = $st_jobDir

# each release has its own directory
multi = True

# the release dir name has the project prepended
projtest = True

EOF
    \mv $st_myasetup.tmp $st_myasetup

    local aSetupLog="${logDir}/aSetup-${st_Template}.log"
# can we do a setup without warnings or errors ?
    st_SetupScriptList=(
	"asetup --input $st_myasetup $st_TagSetup"
    )

    \rm -f $aSetupLog
    touch $aSetupLog
    local st_setupScript=""
    local st_retCode=0
    local st_item
    for st_item in "${st_SetupScriptList[@]}"; do
	st_setupScript="$st_setupScript; $st_item"
	eval $st_item >> $aSetupLog 2>&1
	st_rc=$?
	if [[ $st_rc != 0 ]] && [[ $st_retCode == 0 ]]; then 
	    st_retCode=$st_rc; 
	fi
    done

    local st_rc=$st_retCode
    local st_warnings
    local st_errors
# disable warnings for now as we expect them
#    st_warnings=`\grep -i Warning $aSetupLog`
#    local st_rcWarning=$?
    local st_rcWarning=99
    st_errors=`\grep -i Error $aSetupLog`
    local st_rcError=$?
    \cat $aSetupLog    
    if [[ $st_rc -ne 0 ]] || [[ $st_rcWarning -eq 0 ]] || [[ $st_rcError -eq 0 ]]; then
	\echo "Errors seen with $st_setupScript"
	\echo -e "asetup ...                                                "'[\033[31mFAILED\033[0m]'
	return 64
    fi

    st_ScriptDir=$TestArea

    \echo -e "asetup ... seems ok ...                                   "'[\033[32m  OK  \033[0m]'

    return $st_rc
}


#!----------------------------------------------------------------------------
st_fn_sourceASGSetup()
#! rcSetup the release
#! args:
#!----------------------------------------------------------------------------
{
    \echo 
    \echo "--> Step $st_Step - Setup release"

    local st_rc=0

    st_RcWorkDir="$st_jobDir/$st_RcWorkArea"
    \mkdir -p $st_RcWorkDir
    \cd $st_RcWorkDir

    st_ScriptDir=$st_RcWorkDir

    local rcSetupLog="${logDir}/rcSetup-${st_Template}.log"
# can we do a setup without warnings or errors ?
    st_SetupScriptList=(
	"rcSetup $st_RcTagSetup"
    )

    \rm -f $rcSetupLog
    touch $rcSetupLog
    local st_setupScript=""
    local st_retCode=0
    local st_item
    for st_item in "${st_SetupScriptList[@]}"; do
	st_setupScript="$st_setupScript; $st_item"
	eval $st_item >> $rcSetupLog 2>&1
	st_rc=$?
	if [[ $st_rc != 0 ]] && [[ $st_retCode == 0 ]]; then 
	    st_retCode=$st_rc; 
	fi
    done

    local st_rc=$st_retCode
    local st_warnings
    local st_errors
# disable warnings for now as we expect them
#    st_warnings=`\grep -i Warning $rcSetupLog`
#    local st_rcWarning=$?
    local st_rcWarning=99
    st_errors=`\grep -i Error $rcSetupLog`
    local st_rcError=$?
    \cat $rcSetupLog    
    if [[ $st_rc -ne 0 ]] || [[ $st_rcWarning -eq 0 ]] || [[ $st_rcError -eq 0 ]]; then
	\echo "Errors seen with $st_setupScript"
	\echo -e "rcsetup ...                                               "'[\033[31mFAILED\033[0m]'
	return 64
    fi

    \echo -e "rcsetup ... seems ok ...                                  "'[\033[32m  OK  \033[0m]'

    return $st_rc
}


#!----------------------------------------------------------------------------
st_fn_buildPackages()
# checks out the packages and builds them
#!----------------------------------------------------------------------------
{
    if [ ! -z $st_RcTagSetup ]; then
	st_fn_buildASGPackages
    else
	st_fn_buildAthenaPackages
    fi
    return $?

}


#!----------------------------------------------------------------------------
st_fn_buildAthenaPackages()
# checks out the packages and builds them
#!----------------------------------------------------------------------------
{
    \echo 
    \echo "--> Step $st_Step - Build"

    local st_rc=0

    if [ "$TestArea" != "" ]; then
	\rm -rf $TestArea
	\mkdir -p $TestArea
    fi

    cd $TestArea

    local st_result
    st_result=`type -t st_fn_${st_Template}Checkout`
    if [[ $? -eq 0 ]] && [[ "$st_result" = "function" ]]; then
	st_fn_${st_Template}Checkout
	st_rc=$?
	\echo -ne "template checkout                                         "
	if [ $st_rc -eq 0 ]; then
	    \echo -e '[\033[32m  OK  \033[0m]'    
	else
	    \echo -e '[\033[31mFAILED\033[0m]'    
	    return $st_rc
	fi    
    fi

    setupWorkArea.py 
    st_rc=$?
    \echo -ne "setup PyUtils workarea                                    "
    if [ $st_rc -eq 0 ]; then
	\echo -e '[\033[32m  OK  \033[0m]'    
    else
	\echo -e '[\033[31mFAILED\033[0m]'    
	return $st_rc
    fi    

    st_fn_buildCmt
    st_rc=$?
    \echo -ne "Build cmt                                                 "
    if [ $st_rc -eq 0 ]; then
	\echo -e '[\033[32m  OK  \033[0m]'    
    else
	\echo -e '[\033[31mFAILED\033[0m]'    
	return $st_rc
    fi    
    
    return 0    

}


#!----------------------------------------------------------------------------
st_fn_buildASGPackages()
# checks out the packages and builds them
#!----------------------------------------------------------------------------
{
    \echo 
    \echo "--> Step $st_Step - Build"

    local st_rc=0

    cd $st_RcWorkDir

    local st_result
    st_result=`type -t st_fn_${st_Template}Checkout`
    if [[ $? -eq 0 ]] && [[ "$st_result" = "function" ]]; then
	st_fn_${st_Template}Checkout
	st_rc=$?
	\echo -ne "template checkout                                         "
	if [ $st_rc -eq 0 ]; then
	    \echo -e '[\033[32m  OK  \033[0m]'    
	else
	    \echo -e '[\033[31mFAILED\033[0m]'    
	    return $st_rc
	fi    
    fi

    \echo "rc find_packages ..."
    rc find_packages
    st_rc=$?
    \echo -ne "rc find_packages                                          "    
    if [ $st_rc -eq 0 ]; then
	\echo -e '[\033[32m  OK  \033[0m]'    
    else
	\echo -e '[\033[31mFAILED\033[0m]'    
	return $st_rc
    fi    

    \echo "rc compile ... "
    rc compile
    st_rc=$?
    \echo -ne "rc compile                                                "    
    if [ $st_rc -eq 0 ]; then
	\echo -e '[\033[32m  OK  \033[0m]'    
    else
	\echo -e '[\033[31mFAILED\033[0m]'    
	return $st_rc
    fi    
    
    return 0    

}


#!----------------------------------------------------------------------------
st_fn_buildCmt()
#! compile the cmt release
#!----------------------------------------------------------------------------
{

    local st_rc=0

    cd ${TestArea}/WorkArea/cmt

    \echo "Doing cmt config ..."
    cmt bro cmt config
    st_rc=$?
    \echo -ne "cmt config                                                "
    if [ $st_rc -eq 0 ]; then
	\echo -e '[\033[32m  OK  \033[0m]'    
    else
	\echo -e '[\033[31mFAILED\033[0m]'    
	return $st_rc
    fi    

    \echo "Compiling and linking ..."
    cmt bro make
    st_rc=$?
    \echo -ne "make                                                      "
    if [ $st_rc -eq 0 ]; then
	\echo -e '[\033[32m  OK  \033[0m]'    
    else
	\echo -e '[\033[31mFAILED\033[0m]'    
	return $st_rc
    fi    

    return 0
}


#!----------------------------------------------------------------------------
st_fn_createSetupScript()
#! Create the setup scripts to ease things
#! args:
#!----------------------------------------------------------------------------
{
    \echo 
    \echo "--> Step $st_Step - Generate Script"
    local st_rc=0

    if [ ! -z $st_RcTagSetup ]; then
	cd $st_RcWorkDir
    else
	cd ${TestArea}/WorkArea/run
    fi

    \rm -f ./submitPanda.py
    \ln -s $st_t2SWDir/submitPanda.py ./        

    \rm -f ./purgeJobs.py
    \ln -s $st_t2SWDir/purgeJobs.py ./        

    \rm -f ./testJob.sh 
    \ln -s $st_t2SWDir/testJob.sh ./

    \rm -f ./pandaJob.sh    
    \ln -s $st_t2SWDir/templates/$st_Template/pandaJob.sh ./

    \rm -f ./pandaTestJob.sh
    \ln -s $st_t2SWDir/templates/$st_Template/pandaTestJob.sh ./

    \rm -f ./gangaJob.py
    \ln -s $st_t2SWDir/templates/$st_Template/gangaJob.py

    \rm -f ./gangaTestJob.py
    \ln -s $st_t2SWDir/templates/$st_Template/gangaTestJob.py	

    if [ -e $st_t2SWDir/templates/$st_Template/run.sh ]; then
	\rm -f ./run.sh
	\ln -s $st_t2SWDir/templates/$st_Template/run.sh 
    fi

    \rm -f ./interactive.sh
    \ln -s $st_t2SWDir/templates/$st_Template/interactive.sh

    \rm -f $st_ScriptFile
    \cat <<EOF > $st_ScriptFile
source $st_t2SWDir/do.sh setup $st_Template
EOF

    return $st_rc
}




#!----------------------------------------------------------------------------
st_fn_getDummyInput()
#! Get the dummy input file
#! args:
#!----------------------------------------------------------------------------
{
    \echo 
    \echo "--> Step $st_Step - Get Input File"
    local st_rc=0

    local st_result
    st_result=`type -t st_fn_${st_Template}GetInputFile`
    if [[ $? -eq 0 ]] && [[ "$st_result" = "function" ]]; then
	st_fn_${st_Template}GetInputFile
	st_rc=$?
	\echo -ne "Get Input File                                            "
	if [ $st_rc -eq 0 ]; then
	        \echo -e '[\033[32m  OK  \033[0m]'    
	else
	    \echo -e '[\033[31mFAILED\033[0m]'    
	    return $st_rc
	fi    
    elif [ ! -z $st_dummyInput ]; then
	\mkdir -p $st_DataHome
	env | \grep -e "^ALRB" -e "^ATLAS_LOCAL_" -e "^X509_USER_PROXY" -e "^RUCIO_ACCOUNT" |  env LC_ALL=C \sort | \awk '{print "export "$1""}' | \sed -e 's|=|="|g' -e 's|$|"|g' > $st_workarea/fetchDummyInput.sh
	\echo "$st_t2SWDir/fetchTestData.sh $st_DataHome $st_dummyInput" >> $st_workarea/fetchDummyInput.sh
	chmod +x $st_workarea/fetchDummyInput.sh
	env -i bash -l -c $st_workarea/fetchDummyInput.sh
	if [ $? -ne 0 ]; then
	    return 64
	fi
	local st_dummyDatapath="$st_DataHome/"`\echo $st_dummyInput | \sed -e 's|:|\/|g'`
	if [ "$st_RcWorkDir" = "nonedummy" ]; then
	    \rm -f ${TestArea}/WorkArea/run/AOD.pool.root
	    \ln -s $st_dummyDatapath ${TestArea}/WorkArea/run/AOD.pool.root
	else
	    \rm -f $st_RcWorkDir/AOD.pool.root
	    \ln -s $st_dummyDatapath $st_RcWorkDir/AOD.pool.root
	fi
    fi

    return $st_rc
}


#!----------------------------------------------------------------------------
st_fn_getJobOptions()
#! Get job options file
#! args:
#!----------------------------------------------------------------------------
{
    if [ ! -z $st_RcTagSetup ]; then
	st_fn_getASGJobOptions
    else
	st_fn_getAthenaJobOptions
    fi
    return $?

}


#!----------------------------------------------------------------------------
st_fn_getAthenaJobOptions()
#! Get job options file
#! args:
#!----------------------------------------------------------------------------
{

    \echo 
    \echo "--> Step $st_Step - Get JobOption File"
    cd $TestArea/WorkArea/run

    local st_rc=0

    local st_result
    st_result=`type -t st_fn_${st_Template}GetJobOptions`
    if [[ $? -eq 0 ]] && [[ "$st_result" = "function" ]]; then
	st_fn_${st_Template}GetJobOptions
	st_rc=$?
	\echo -ne "Get Job Options File                                      "
	if [ $st_rc -eq 0 ]; then
	        \echo -e '[\033[32m  OK  \033[0m]'    
	else
	    \echo -e '[\033[31mFAILED\033[0m]'    
	    return $st_rc
	fi    
    fi

    return $st_rc

}


#!----------------------------------------------------------------------------
st_fn_getASGJobOptions()
#! Get job options file
#! args:
#!----------------------------------------------------------------------------
{

    \echo 
    \echo "--> Step $st_Step - Get JobOption File"
    
    cd $st_RcWorkDir

    local st_rc=0

    local st_result
    st_result=`type -t st_fn_${st_Template}GetJobOptions`
    if [[ $? -eq 0 ]] && [[ "$st_result" = "function" ]]; then
	st_fn_${st_Template}GetJobOptions
	st_rc=$?
	\echo -ne "Get Job Options File                                      "
	if [ $st_rc -eq 0 ]; then
	        \echo -e '[\033[32m  OK  \033[0m]'    
	else
	    \echo -e '[\033[31mFAILED\033[0m]'    
	    return $st_rc
	fi    
    fi

    return $st_rc

}


#!----------------------------------------------------------------------------
#! main
#!----------------------------------------------------------------------------

if [ ! -e ${st_t2SWDir}/create.sh ]; then
    \echo -e "You need to run this from the siteTests directory         "'[\033[31mFAILED\033[0m]'
    exit 64
fi

st_shortopts="h"
st_longopts="help,myWorkHome:,startStep:,template:,voms:,alrbOpts:"
st_opts=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_parseOptions.sh bash $st_shortopts $st_longopts $st_progname "$@"`
if [ $? -ne 0 ]; then
    \echo $st_opts 1>&2
    exit 64
fi
eval set -- "$st_opts"

let st_startStep=0
st_Voms="atlas"
st_ScriptDir="nonedummy"
st_AlrbOpts=""

st_workarea="nonedummy"
st_RcWorkDir="nonedummy"
st_DataHome="$st_MyWorkHome/Data"

while [ $# -gt 0 ]; do
    : debug: $1
    case $1 in
        -h|--help)
            st_fn_initialSiteTestsSetupHelp
            exit 0
            ;;
        --myWorkHome)
            st_MyWorkHome=`eval "\echo $2"`
            shift 2
            ;;
        --startStep)
            let st_startStep=$2
            shift 2
            ;;
        --template)
            st_Template=$2
            shift 2
            ;;
        --voms)
            st_Voms=$2
            shift 2
            ;;
	--alrbOpts)
	    st_AlrbOpts=$2
	    shift 2
	    ;;
	--)
            shift
            break
            ;;
        *)
            \echo "Internal Error: option processing error: $1" 1>&2
            exit 1
            ;;
    esac
done

# do everything in a sub-shell to avoid contaminating
(

    if [ ! -e "$st_userDir/workdirs" ]; then
	touch $st_userDir/workdirs
    fi

    if [ -e "$st_t2SWDir//templates/$st_Template/config.sh" ]; then
	source "$st_t2SWDir//templates/$st_Template/config.sh"
    else
	\echo "Error: Unable to find config file for template $st_Template"
	exit 64
    fi
    
    if [ -e "$st_t2SWDir/templates/$st_Template/build.sh" ]; then	
	source "$st_t2SWDir/templates/$st_Template/build.sh"
    fi

    shopt -s expand_aliases
    
    let st_Step=0    
    
    mkdir -p $st_MyWorkHome
    
    logDir=${st_MyWorkHome}/logs
    mkdir -p $logDir

    st_jobDir="$st_MyWorkHome/jobs/$st_Template"
    mkdir -p $st_jobDir
    
    clear

    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh $st_AlrbOpts --quiet --noLocalPostSetup

    if [ ! -d $ALRB_tmpScratch ]; then
	\echo "Error: temporary scratch dir (ALRB_tmpScratch) does not exist"
	exit 64
    fi
    st_workarea=`\mktemp -d $ALRB_tmpScratch/siteTests.XXXXXX`
    if [ $? -ne 0 ]; then
	\echo "Failed to create dir"
	exit 64
    fi

    
    let st_Step+=1
    st_fn_checkHost
    st_rc=$?
    if [ $st_rc != 0 ]; then
	st_fn_addSummary "$st_Step" "Check host" "Failed"
	\echo "Failed at step $st_Step"
	st_fn_printSummary
	exit $st_rc
    else
	st_fn_addSummary "$st_Step" "Check host" "OK"
    fi
    
    
    let st_Step+=1
    if [ $st_Step -ge $st_startStep ]; then
	st_fn_checkSpace
	st_rc=$?
	if [ $st_rc != 0 ]; then
	    st_fn_addSummary "$st_Step" "Check disk space" "Failed"
	    \echo "Failed at step $st_Step"
	    st_fn_printSummary
	    exit $st_rc
	else
	    st_fn_addSummary "$st_Step" "Check disk space" "OK"
	fi
    fi
    
    
    let st_Step+=1
    if [ $st_Step -ge $st_startStep ]; then
	st_fn_checkGridCertificates
	st_rc=$?
	if [ $st_rc != 0 ]; then
	    st_fn_addSummary "$st_Step" "Check grid certificates" "Failed"
	    \echo "Failed at step $st_Step"
	    st_fn_printSummary
	    exit $st_rc
	else
	    st_fn_addSummary "$st_Step" "Check host" "OK"
	fi
    fi
    
    
    let st_Step+=1
    if [ $st_Step -ge $st_startStep ]; then
	st_fn_createGangaSetup
	st_rc=$?
	if [ $st_rc != 0 ]; then
	    st_fn_addSummary "$st_Step" "Create ganga configuration" "Failed"
	    \echo "Failed at step $st_Step"
	    st_fn_printSummary
	    exit $st_rc
	else
	    st_fn_addSummary "$st_Step" "Create ganga configuration" "OK"
	fi
    fi
    
    
    let st_Step+=1
    st_fn_sourceSetup 
    st_rc=$?
    if [ $st_rc != 0 ]; then
	st_fn_addSummary "$st_Step" "Setup release" "Failed"
	\echo "Failed at step $st_Step"
	st_fn_printSummary
	exit $st_rc
    else
	st_fn_addSummary "$st_Step" "Setup release" "OK"
    fi
    
    
    let st_Step+=1
    if [ $st_Step -ge $st_startStep ]; then
	st_fn_buildPackages 
	st_rc=$?
	if [ $st_rc != 0 ]; then
	    st_fn_addSummary "$st_Step" "Build" "Failed"
	    \echo "Failed at step $st_Step"
	    st_fn_printSummary
	    exit $st_rc
	else
	    st_fn_addSummary "$st_Step" "Build" "OK"
	fi
    fi
    
    
    let st_Step+=1
    if [ $st_Step -ge $st_startStep ]; then
	st_fn_getDummyInput
	st_rc=$?
	if [ $st_rc != 0 ]; then
	    st_fn_addSummary "$st_Step" "Get input file" "Failed"
	    \echo "Failed at step $st_Step"
	    st_fn_printSummary
	    exit $st_rc
	else
	    st_fn_addSummary "$st_Step" "Get input file" "OK"
	fi
    fi

    
    let st_Step+=1
    if [ $st_Step -ge $st_startStep ]; then
	st_fn_getJobOptions
	st_rc=$?
	if [ $st_rc != 0 ]; then
	    st_fn_addSummary "$st_Step" "Get job options" "Failed"
	    \echo "Failed at step $st_Step"
	    st_fn_printSummary
	    exit $st_rc
	else
	    st_fn_addSummary "$st_Step" "Get job options" "OK"
	fi
    fi
    
    
    let st_Step+=1
    if [ $st_Step -ge $st_startStep ]; then
	st_ScriptFile=${st_ScriptDir}/st-setup.sh
	st_fn_createSetupScript
	st_rc=$?
	if [ $st_rc != 0 ]; then
	    st_fn_addSummary "$st_Step" "Create setup scipt" "Failed"
	    \echo "Failed at step $st_Step"
	    st_fn_printSummary
	    exit $st_rc
	else
	    st_fn_addSummary "$st_Step" "Create setup scipt" "OK"
	fi
    fi

    st_result=`\grep "$st_Template:$st_ScriptDir" $st_userDir/workdirs 2>&1`
    if [ $? -ne 0 ]; then
	\echo "$st_Template:$st_ScriptDir" >> $st_userDir/workdirs
    fi
    
    
# if we reach this step, all is good.
    st_fn_printSummary

    \echo "

Completed ${st_progname}; you seem to be ready to use the release.
To use this release whenever you login, simply type
  source $st_t2SWDir/do.sh setup $st_Template

"

if [ "$st_workarea" != "nonedummy" ]; then
    \rm -rf $st_workarea
fi
    
)


#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! regenerateGangaRc.sh
#!
#! Regenerate gangarc file
#!
#! Usage: 
#!     regenerateGangaRc.sh
#!
#! History:
#!   19jun13: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

source  $ATLAS_LOCAL_ROOT_BASE/swConfig/ganga/generateGangarc.sh --outDir=$ST_gangaDir --outFilename=gangarc --gangadir="$ST_gangaDir/gangadir"

#!----------------------------------------------------------------------------
#!
#! do.sh 
#!
#! do many things with siteTests
#!  create, setup templates 
#!
#! Usage: 
#!     source do.sh --help
#!
#! History:
#!   19Nov08: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

st_UserDir="$HOME/.SiteTests"
st_progname="do"

if [ ! -e $st_UserDir/config.sh ]; then
    \echo "Error: did you run updateSiteTests.sh when you installed siteTests ?"
    \echo "  Please rerun updateSiteTests.sh in the siteTests dir"
    return 64
fi
source $st_UserDir/config.sh
source $st_t2SWDir/defaultTemplate.sh

#!----------------------------------------------------------------------------
st_fn_doHelp()
#!----------------------------------------------------------------------------
{
    \cat <<EOF

Usage: 

    $st_progname create [template]
      specify template if not default

    $st_progname setup <template>
      setup template

    $st_progname list
      list available templates

    $st_progname update
      update siteTests

    Options (to override defaults) are:
    -h  --help                    print this help message

EOF
}



#!----------------------------------------------------------------------------
st_fn_checkCreatedTemplate()
#!----------------------------------------------------------------------------
{

    local st_cand=$1 
    shift

    local st_createdItem
    local st_createdTemplateAr
    local st_created="NO"
    st_createdItemAr=( `\grep "$st_cand" $st_UserDir/workdirs 2>&1` )
    if [ $? -eq 0 ]; then
	for st_createdItem in ${st_createdItemAr[@]}; do
	    local st_dir=`\echo $st_createdItem | \cut -f 2 -d ":"`
	    if [ ! -d $st_dir ]; then
		\mv $st_UserDir/workdirs $st_UserDir/workdirs.old
		\egrep -v $st_createdItem $st_UserDir/workdirs.old > $st_UserDir/workdirs
	    else
		st_created="YES"
	    fi
	done
    fi
    
    if [ "$st_created" = "YES" ]; then
	return 0
    else
	return 1
    fi

    return 1

}


#!----------------------------------------------------------------------------
st_fn_list()
#!----------------------------------------------------------------------------
{

    printf " %-10s %-60s\n" "Template" "Description"	
    local st_templateList 
    st_templateList=( `\find $st_t2SWDir/templates -name config.sh | env LC_ALL=C \sort` )    
    local st_item
    local st_createdItem
    local st_createdTemplateAr
    for st_item in ${st_templateList[@]}; do
	local st_isDefault=""
	eval source $st_item
	if [ "$st_DefaultTemplate" = "$st_Template" ]; then
	    local st_isDefault="D"
	fi
	st_fn_checkCreatedTemplate $st_Template
	if [ $? -eq 0 ]; then
	    local st_created="C"
	else
	    local st_created=""
	fi
	printf " %-10s %-60s\n" "$st_Template $st_isDefault$st_created" "$st_Description"
	st_fn_cleanup
    done
    
    return 0
}


#!----------------------------------------------------------------------------
st_fn_create()
#!----------------------------------------------------------------------------
{

    local st_cand=$1
    shift

    if [ ! -d $st_t2SWDir/templates/$st_cand ]; then
	\echo "Error: Template $st_cand does not exist."
	return 64
    fi

    local st_doContinue
    st_fn_checkCreatedTemplate $st_cand
    if [ $? -eq 0 ]; then
	\echo "Tempkate $st_cand is already created."
	\echo -n " Do you want to recreate it ? (yes[no]) : "
	read st_doContinue
	case $st_doContinue in
	    [yY][eE][sS] ) \echo "Continuing ...";;
            [nN][oO] ) \echo "Aborting now ..."; return 64;;
	    * ) \echo "Interpreting $st_doContinue to mean no. Aborting now ..."; return 64;;
	esac
    fi

    $st_t2SWDir/create.sh --template $st_cand

    return $?

}


#!----------------------------------------------------------------------------
st_fn_releaseSetup()
#!----------------------------------------------------------------------------
{

    local st_cand=$1
    export ST_myWorkHome=$2
    shift 2

    export ST_t2SWDir=$st_t2SWDir
    
    if [ -z $ATLAS_LOCAL_ROOT_BASE ]; then
	export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
	alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
    fi
    
    source $ST_t2SWDir/templates/$st_cand/config.sh

    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q -p

    if [ ! -z $st_RcTagSetup ]; then
	cd $ST_myWorkHome
	source $ATLAS_LOCAL_RCSETUP_PATH/rcSetup.sh $st_RcTagSetup
	if [ $? -ne 0 ]; then
	    return 64
	fi	
    else
	source $AtlasSetup/scripts/asetup.sh --input $ST_myWorkHome/../.asetup $st_TagSetup
	if [ $? -ne 0 ]; then
	    return 64
	fi	
	cd $ST_myWorkHome/WorkArea/cmt
	source setup.sh
	if [ $? -ne 0 ]; then
	    return 64
	fi	
	cd ../run
    fi

    if [ -e $ST_t2SWDir/templates/$st_Template/postSetup.sh ]; then
	source $ST_t2SWDir/templates/$st_Template/postSetup.sh 
    fi

    local st_gangaDir=`\echo $ST_myWorkHome | \sed -e 's|/jobs.*||g'`

    export ST_gangaDir=$st_gangaDir
    
    alias gangaJobs='source $ST_t2SWDir/submitGangaJobs.sh'
    alias pandaJobs='source $ST_t2SWDir/submitPandaJobs.sh'
    if [ ! -z $st_RcTagSetup ]; then
	alias interactive='source $ST_myWorkHome/interactive.sh'
    else
	alias interactive='source $ST_myWorkHome/WorkArea/run/interactive.sh'
    fi

    \echo "

This is template $st_Template :  $st_Description
Type:
  gangaJobs        to submit jobs to the grid via Ganga
  pandaJobs        to submit jobs to the grid via Panda
  interactive      to run an interactive test 

"  
   
    return 0
}


#!----------------------------------------------------------------------------
st_fn_setup()
#!----------------------------------------------------------------------------
{

    local st_cand=$1
    shift

    local st_setupScript=""
    local st_createdItemAr
    local st_createdItem    
    local st_dir=""
    st_createdItemAr=( `\grep "$st_cand" $st_UserDir/workdirs 2>&1` )
    if [ $? -eq 0 ]; then
	for st_createdItem in ${st_createdItemAr[@]}; do
	    st_dir=`\echo $st_createdItem | \cut -f 2 -d ":"`
	    if [ ! -d $st_dir ]; then
		\mv $st_UserDir/workdirs $st_UserDir/workdirs.old
		\egrep -v $st_createdItem $st_UserDir/workdirs.old > $st_UserDir/workdirs
	    elif [ -e $st_dir/st-setup.sh ]; then
		st_setupScript="$st_dir/st-setup.sh"
		break
	    fi
	done
    fi


    if [ "$st_setupScript" != "" ]; then
	st_fn_releaseSetup $st_cand $st_dir
	if [ $? -ne 0 ]; then
	    \echo "Error: Failed to setup template $st_cand" 
	    return 64
	fi
    else
	\echo "Error: cannot setup template $st_cand"
	if [ -e $st_t2SWDir/templates/$st_cand/config.sh ]; then
	    \echo "  You need to create it first."
	fi
	return 64
    fi

    return 0

}


#!----------------------------------------------------------------------------
st_fn_cleanup() 
#!----------------------------------------------------------------------------
{
    
    unset st_Template
    unset st_TagSetup
    unset st_inDS
    unset st_dummyInput
    unset st_Description
    unset st_RcTagSetup
    unset st_RcWorkArea

    return 0
}


#!----------------------------------------------------------------------------
#! main
#!----------------------------------------------------------------------------

st_fn_cleanup

st_shortopts="h"
st_longopts="help"
st_opts=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_parseOptions.sh bash $st_shortopts $st_longopts $st_progname "$@"`
if [ $? -ne 0 ]; then
    \echo $st_opts 1>&2
    return 64
fi
eval set -- "$st_opts"

while [ $# -gt 0 ]; do
    : debug: $1
    case $1 in
        -h|--help)
            st_fn_doHelp
            return 0
            ;;
	--)
            shift
            break
            ;;
        *)
            \echo "Internal Error: option processing error: $1" 1>&2
            return 1
            ;;
    esac
done

st_result=""
if [ "$*" = "" ]; then
    st_fn_doHelp    
else
    st_result=`\echo $* | \cut -f 1 -d " "`    
    shift
    if [ "$st_result" = "help" ]; then
	st_fn_doHelp
    elif [ "$st_result" = "create" ]; then
	if [ "$*" != "" ]; then	    
	    st_fn_create $1
	else
	    st_fn_create $st_DefaultTemplate
	fi
    elif [ "$st_result" = "setup" ]; then
	if [ "$*" != "" ]; then	    
	    st_fn_setup $1
	else
	    st_fn_setup $st_DefaultTemplate
	fi
    elif [ "$st_result" = "list" ]; then
	st_fn_list
    elif [ "$st_result" = "update" ]; then
	$st_t2SWDir/updateSiteTests.sh 
    else
	st_fn_doHelp
	return 64
    fi    
    st_rc=$?
fi

unset st_result st_shortopts st_longopts st_opts st_t2SWDir st_UserDir st_progname

return $st_rc

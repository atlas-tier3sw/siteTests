#!----------------------------------------------------------------------------
st_fn_775GetJobOptions()
# get job options file
#!----------------------------------------------------------------------------
{
    local st_rc=0    
    
    get_files AthExample.py 
    st_rc=$?
    if [ $st_rc -eq 0 ]; then
	\cp AthExample.py AthExample.py.orignal
	\sed -e "s/svcMgr.EventSelector.InputCollections = \[testFile\]/svcMgr.EventSelector.InputCollections = \[ 'AOD.pool.root' \]/g" AthExample.py > AthExample.py.new
	st_rc=$?
	if [ $st_rc -eq 0 ]; then
	    \mv AthExample.py.new AthExample.py
	fi
    fi
    \echo -ne "fetching job options file                                 "
    if [ $st_rc -eq 0 ]; then
	\echo -e '[\033[32m  OK  \033[0m]'    
    else
	\echo -e '[\033[31mFAILED\033[0m]'    
	return $st_rc
    fi    

    
    return 0    

}


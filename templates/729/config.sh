st_Template="729"

st_TagSetup="20.1.5.7,AtlasDerivation"

st_inDS="mc15_13TeV:mc15_13TeV.*.merge.AOD.*_r6630_r6264*,mc15_13TeV:mc15_13TeV.*.merge.AOD.*_r6633_r6264*"

st_dummyInput="mc15_13TeV:AOD.05536542._000001.pool.root.1"

st_Description="(deprecated, do not use) AFT AtlasDerivation 20.1.5.7"

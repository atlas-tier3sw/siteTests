j = Job()
j.name = st_gangaName
j.application=Athena()
j.application.useNoAthenaSetup = True 
j.application.atlas_exetype='EXE'
j.application.options=" ".join([st_script,"| tee" ,"out.txt"])
j.application.useRootCore = True
j.application.athena_compile = True
j.application.atlas_dbrelease = ''
j.outputdata=DQ2OutputDataset()
j.outputdata.outputdata = ["out.txt"]
j.outputdata.datasetname = st_outputds
j.backend=Jedi()
j.backend.nobuild = True
j.backend.site=st_queue
j.submit()

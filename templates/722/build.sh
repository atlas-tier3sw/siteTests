#!----------------------------------------------------------------------------
st_fn_722Checkout()
# checks out the packages and builds them
#!----------------------------------------------------------------------------
{
    local st_rc=0

    cd $st_RcWorkDir

    \echo "Checking out QuickAna-00-00-60 ..."
    rc checkout_pkg atlasoff/PhysicsAnalysis/TopPhys/QuickAna/tags/QuickAna-00-00-60 QuickAna
    st_rc=$?
   
    \echo -ne "Checking out QuickAna-00-00-60 ...                        "
    if [ $st_rc -eq 0 ]; then
	\echo -e '[\033[32m  OK  \033[0m]'    
    else
	\echo -e '[\033[31mFAILED\033[0m]'    
	return $st_rc
    fi    
    
    return $st_rc

}


#!----------------------------------------------------------------------------
st_fn_722GetJobOptions()
# get job options file
#!----------------------------------------------------------------------------
{
    local st_rc=0

    cd $st_RcWorkDir
    \rm -f ./input.txt
    \echo "AOD.pool.root" > ./input.txt

    return $st_rc
}
import shutil
shutil.rmtree('output',ignore_errors=True)

j = Job()
j.name = st_gangaName
j.application=Athena()
j.application.useNoAthenaSetup = True 
j.application.atlas_exetype = 'EXE'
j.application.option_file=['run.sh %IN' ]
j.application.useRootCore = True
j.application.athena_compile = True
j.application.atlas_cmtconfig = 'x86_64-slc6-gcc48-opt'
j.application.max_events = 50
j.application.atlas_dbrelease = ''
j.application.useRootCoreNoBuild = False
j.inputdata=DQ2Dataset()
j.inputdata.dataset=st_dataset
j.inputdata.number_of_files=1
j.outputdata=DQ2OutputDataset()
j.outputdata.outputdata=['lin_met_pt.gif']
j.outputdata.datasetname = st_outputds
j.backend=Jedi()
j.backend.site=st_queue
j.backend.requirements.rootver='6.02/05'
j.submit()

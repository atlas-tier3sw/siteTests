\rm -rf output AthSummary.txt

st_cmd="prun --exec \"run.sh %IN\" --nEvents=50 --nFiles=1 --inDS $st_dataset --outDS=$st_outputds --useRootCore --outputs=AthSummary.txt,lin_met_pt.gif --site=$st_queue --rootVer=6.02/05 --cmtConfig=x86_64-slc6-gcc48-opt"

\echo "$st_cmd"
eval $st_cmd
unset st_cmd



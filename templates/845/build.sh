#!----------------------------------------------------------------------------
st_fn_845GetJobOptions()
# get job options file
#!----------------------------------------------------------------------------
{
    local st_rc=0    
    
    get_files Reco_tf.py
    st_rc=$?
    \echo -ne "fetching job options file                                 "
    if [ $st_rc -eq 0 ]; then
	\echo -e '[\033[32m  OK  \033[0m]'    
    else
	\echo -e '[\033[31mFAILED\033[0m]'    
	return $st_rc
    fi    
    
    return 0    

}


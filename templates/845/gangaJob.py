j = Job()
j.name = st_gangaName
j.application=Athena()        
j.application.useNewTRF = True
j.application.option_file="".join([st_runDir,'/Reco_tf.py'])
j.application.max_events=50
j.application.options = '--inputAODFile %IN --outputDAODFile test.pool.root --reductionConf HIGG3D1'
j.application.atlas_exetype='TRF' 
j.application.prepare()
j.inputdata=DQ2Dataset()
j.inputdata.dataset=st_dataset
j.inputdata.number_of_files=1
j.outputdata=DQ2OutputDataset()
j.outputdata.outputdata=['DAOD_HIGG3D1.test.pool.root']
j.outputdata.datasetname = st_outputds
j.backend=Jedi()
j.backend.nobuild = True
j.backend.site=st_queue
j.submit()


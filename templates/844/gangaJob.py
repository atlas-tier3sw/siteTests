j = Job()
j.name = st_gangaName
j.application=Athena()
j.application.option_file="".join([st_runDir,'/AthExample.py'])
j.application.max_events=100
j.application.athena_compile=True
j.application.prepare()
j.application.atlas_release = '2.3.48'
j.inputdata=DQ2Dataset()
j.inputdata.dataset=st_dataset
j.inputdata.number_of_files=1
j.outputdata=DQ2OutputDataset()
j.outputdata.datasetname = st_outputds
j.backend=Jedi()
j.backend.site=st_queue
j.submit()

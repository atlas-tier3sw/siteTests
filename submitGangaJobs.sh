#!----------------------------------------------------------------------------
#!
#! submitGangaJobs.sh
#!
#! A wrapper for submitGanga.py
#!
#! Usage: 
#!     source submitGangaJobs.sh
#!
#! History:
#!   19Nov08: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

\echo -e '\033[34m'"
  gangadir, gangarc is located at $ST_gangaDir

  Ganga will start now.  You may be prompted for the grid proxy password.

  (For SW testing - not for site admins to test !  
  Site admins, use Panda directly)
      execfile('submitPanda.py')
  will use the Panda backend from within Ganga.

  Note: if you want to cleanup your old jobs from the ganga listings, type
      execfile('purgeJobs.py')
  You should do this periodically to free up your disk space.

"'\033[0m'

if [[ -z $ATLAS_LOCAL_ROOT_BASE ]] && [[ -d "/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase" ]] ; then
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
fi

if [ -z $SITETESTS_DEFINED_GANGA ]; then
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
    lsetup ganga
    export SITETESTS_DEFINED_GANGA="YES"
fi

if [ ! -d $ALRB_tmpScratch ]; then
    \echo "Error: temporary scratch dir (ALRB_tmpScratch) does not exist"
    exit 64
fi
st_Workarea=`\mktemp -d $ALRB_tmpScratch/siteTests.XXXXXX`
if [ $? -ne 0 ]; then
    \echo "Failed to create dir"
    exit 64
fi

export ST_workarea=$st_Workarea
export ST_template=$st_Template
export ST_inDS=$st_inDS

appendPath PYTHONPATH `pwd`

ganga --config=$ST_gangaDir/gangarc

# cleanup
\echo "Now cleaning up ... "

\rm -rf $st_Workarea
unset ST_workarea st_Workarea

\rm -f $ST_gangaDir/gangadir/workspace/`whoami`/LocalAMGA/*.gz
    

#! /bin/bash

st_downloadDir=$1
st_dataset=$2

st_datapath="$st_downloadDir/"`\echo $st_dataset | \sed -e 's|:|\/|g'`
if [ -e $st_datapath ]; then
    \echo "Skipping download of $st_dataset as it exists"
    exit 0
fi


source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh $st_AlrbOpts --quiet --noLocalPostSetup

lsetup rucio -q

# get a proxy if needed
st_doProxy=""
voms-proxy-info -exists > /dev/null 2>&1
if [ $? -ne 0 ]; then
    st_doProxy="Y"
else
    st_result=`voms-proxy-info --actimeleft 2>&1 | \grep -e "^[0-9]*$"`
    if [[ $? -ne 0 ]] || [[ "st_result" = "" ]]; then
	st_doProxy="Y"
    fi
fi

if [ "$st_doProxy" = "Y" ]; then
    \echo ""
    \echo "Getting a valid proxy ..."
    voms-proxy-init -voms atlas -valid 96:0
    if [ $? -ne 0 ]; then
	exit 64
    fi
fi

rucio get --dir $st_downloadDir $st_dataset
exit $?
